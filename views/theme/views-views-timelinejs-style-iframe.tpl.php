<?php
/**
 * @file views-views-json-style.tpl.php
 * This template file is never used. The switch for choosing among the different
 * views_views_json_* themes based on format is done in
 * views_plugin_style_json->theme_functions()
 *
 * - $view: The View object.
 *   $options: Array of plugin style options
 *
 * @ingroup views_templates
 */

$url = $options['datasource_url'];
if(!$options['absolute_url']) {
  $url = url(ltrim($options['datasource_url'], '/'), array('absolute' => true));
}

$startAtEnd = 'true';
if(!$options['start_at_end']) {
  $startAtEnd = 'false';
}

$width = $options['width'];
$height = $options['height'];
$jsPath = drupal_get_path('module', 'views_timelinejs') . '/js';
$cssPath = drupal_get_path('module', 'views_timelinejs') . '/css';

drupal_add_js(drupal_get_path('module', 'views_timelinejs') .'/js/storyjs-embed.js');
$rand = 'timeline-embed-'.rand(1,100);
?>

<div id="<?php echo $rand; ?>"></div>
<script type="text/javascript">
  jQuery(document).ready(function() {

    createStoryJS({
      type:       'timeline',
      width:      '<?php echo $width; ?>',
      height:     '<?php echo $height; ?>',
      source:     '<?php echo $url; ?>',
      embed_id:   '<?php echo $rand; ?>', // ID of the DIV you want to load the timeline into
      css:        '<?php echo $cssPath; ?>/timeline.css',
      js:         '<?php echo $jsPath; ?>/timeline-min.js',
      start_at_end: <?php echo $startAtEnd; ?>,
      start_zoom_adjust: <?php echo $options['start_zoom_adjust']; ?>
    });
  });
</script>

