<?php

/**
 * @file
 * Views theme to render view fields as JSON.
 *
 * - $view: The view in use.
 * - $rows: Array of row objects as rendered by _views_json_render_fields
 * - $attachment: Not used currently
 * - $options: The options for the style passed in from the UI.
 *
 * @ingroup views_templates
 * @see views_json.views.inc
 */
function template_preprocess_views_views_timelinejs_style(&$vars) {
  $view = $vars["view"];
  $rows = $vars["rows"];
  $options = $vars["options"];
  $base = $view->base_table;
  $root_object = $options["root_object"];
  $top_child_object = $options["top_child_object"];
  $plaintext_output = $options["plaintext_output"];
  $objects = array();

  // Create bitmask for json_encode.
  $option_defs = $vars['view']->style_plugin->option_definition();
  $bitmasks = $option_defs['encoding']['contains'];
  $bitmask = NULL;
  foreach($bitmasks as $mask_key => $_bitmask) {
    if(isset($options[$mask_key]) && $options[$mask_key] && !is_array($options[$mask_key])) {
      $bitmask = $bitmask | constant($_bitmask['bitmask']);
    }
  }
  $vars['bitmask'] = $bitmask;

  $earliestDate = time();
  $latestDate = time();
  foreach ($rows as $row) {

    $object = array();
    /* Convert the $rows into a hierachial key=>value array */
    foreach ($row as $field) {
      if ($options["field_output"] == "normal") {
        if ($field->label)
          $label = $plaintext_output ? strip_tags($field->label) : $field->label;
        else {
          $label = $plaintext_output ? strip_tags($field->id) : $field->id;
        }
        if (!$field->is_multiple) {
          $content = $plaintext_output ? strip_tags($field->content) : $field->content;
        }
        else {
          $content = array();
          foreach ($field->content as $n => $oc) $content[$n] = ($plaintext_output ? strip_tags($oc) : $oc);
        }
      }
      elseif ($options["field_output"] == "raw") {
        $label = $plaintext_output ? strip_tags($field->id) : $field->id;
        if (!$field->is_multiple) {
          $content = $plaintext_output ? strip_tags($field->raw) : $field->raw;
        }
        else {
          $content = array();
          foreach ($field->raw as $n => $oc) $content[$n] = $plaintext_output ? strip_tags($oc) : $oc;
        }
      }

      if($label == 'startDate') {
        $date = strtotime($content);
        $content = date('Y,m,d', $date);
        if($earliestDate >= $date) {
          $earliestDate = $date;
        }
      }
      if ($label == 'endDate') {
        $date = strtotime($content);
        $content = date('Y,m,d', $date);
        if($latestDate <= $date) {
          $latestDate = $date;
        }
      }
      // check if user wants nested arrays
      $object[$label] = $content;
    }
    $objects[] = $object;
  }

  // check if user wants nested arrays
  $timeline = array(
    'headline' => !empty($vars['title']) ? $vars['title'] : 'Timeline',
    'type' => 'default',
    //'startDate' => date('Y,m,d', $earliestDate),
    'text' => 'Intro text here',
    'date' => $objects,
  ) ;
  $vars['rows'] = array('timeline' =>  $timeline);
}

function template_preprocess_views_views_timelinejs_style_iframe(&$vars) {
}
