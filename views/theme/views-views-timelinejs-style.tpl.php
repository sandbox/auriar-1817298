<?php
/**
 * @file views-views-json-style.tpl.php
 * This template file is never used. The switch for choosing among the different
 * views_views_json_* themes based on format is done in
 * views_plugin_style_json->theme_functions()
 *
 * - $view: The View object.
 * - $rows: Array of row objects as rendered by _views_json_render_fields
 *   $options: Array of plugin style options
 *
 * @ingroup views_templates
 */

if ($view->override_path) {
  // We're inside a live preview where the JSON is pretty-printed.
  $json = _views_json_encode_formatted($rows);
  print "<code>$json</code>";
}
else {

  $json = _views_json_json_encode($rows, $bitmask);
  if ($options['remove_newlines']) {
    $json = preg_replace(array('/\\\\n/'), '', $json);
  }

  if ($options['using_views_api_mode']) {
    // We're in Views API mode.
    print $json;
  }
  else {
    // We want to send the JSON as a server response so switch the content
    // type and stop further processing of the page.
    $content_type = ($options['content_type'] == 'default') ? 'application/json' : $options['content_type'];
    drupal_add_http_header("Content-Type", "$content_type; charset=utf-8");
    print $json;
    //Don't think this is needed in .tpl.php files: module_invoke_all('exit');
    exit;
  }
}
