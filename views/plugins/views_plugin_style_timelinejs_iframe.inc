<?php
/**
 * @file
 * Implements views_plugin_style for views_json
 */

/**
 * Implements views_plugin_style
 */
class views_plugin_style_timelinejs_iframe extends views_plugin_style {
  /**
   * Implements views_plugin_style::option_definition
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['datasource_url'] = array('default' => '', 'translatable' => FALSE);
    $options['absolute_url'] = array('default' => 0, 'translatable' => FALSE);
    $options['width'] = array('default' => '100%', 'translatable' => FALSE);
    $options['height'] = array('default' => '600', 'translatable' => FALSE);
    $options['start_at_end'] = array('default' => '1', 'translatable' => FALSE);
    $options['start_zoom_adjust'] = array('default' => '2', 'translatable' => FALSE);

    return $options;
  }

  /**
   * Provide a form for setting options.
   */
  function options_form(&$form, &$form_state) {

    $form['datasource_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Datasource URL'),
      '#default_value' => $this->options['datasource_url'],
      '#description' => t('URL to valid TimelineJS'),
    );

    $form['absolute_url'] = array(
      '#type' => 'checkbox',
      '#title' => t('Absolute URL'),
      '#default_value' => $this->options['absolute_url'],
      '#description' => t('Check if URL provided is absolute, otherwise put relative path to this site.'),
    );

    $form['width'] = array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#default_value' => $this->options['width'],
      '#description' => t('Percentage or numeric. Do not include px'),
    );

    $form['height'] = array(
      '#type' => 'textfield',
      '#title' => t('Height'),
      '#default_value' => $this->options['height'],
      '#description' => t('Percentage or numeric. Do not include px'),
    );

    $form['start_at_end'] = array(
      '#type' => 'checkbox',
      '#title' => t('Start At End'),
      '#default_value' => $this->options['start_at_end'],
      '#description' => t('Start at end, right hand side of timeline.'),
    );

    $form['start_zoom_adjust'] = array(
      '#type' => 'textfield',
      '#title' => t('Zoom level'),
      '#default_value' => $this->options['start_zoom_adjust'],
      '#description' => t('Zoom level to start at.'),
    );


  }

  /**
   * Implementation of view_style_plugin::theme_functions(). Returns an array of theme functions to use
   * for the current style plugin
   * @return array
   */
  function theme_functions() {
    $options = $this->options;
    $hook = 'views_views_timelinejs_style_iframe';
    return views_theme_functions($hook, $this->view, $this->display);
  }

  /**
   * Implementation of views_style_plugin::additional_theme_functions(). Returns empty array.
   * @return array
   */
  function additional_theme_functions() {
    return array();
  }

  /**
   * Implementation of view_style_plugin::render()
   */
  function render() {
    $view = $this->view;
    $options = $this->options;
    //$field = $view->field;

    /*
    $rows = array();
    foreach ($view->result as $count => $row) {
      $view->row_index = $count;
      $rows[] = _views_json_render_fields($view, $row);
    }
    unset($view->row_index);
    */

    return theme($this->theme_functions(), array('view' => $view, 'options' => $options));
  }
}
